Hopper

This is a project that I'm making just for fun. I just want to say that before I get any criticising comments by negative people ;)

Hopper is a server modification for MinecraftServer. Its true that this has already been done, we have already an amazing server mod called CraftBukkit. This mod has lots of similarities, one is the way it suports varius plugins. But there are some small difrencies. One is that it use XML instead of YAML. Some might think that YAML is better since its easier to read, but since XML parsers are include in the Java standard API I'm using XML instead. Another difrence is that messages sent to players has the option to be translated into the serves local language. All the plugin has to provide is a proparty file containing that language. Another difrence is that all plugins are limited to specific worlds specified by the server owner. A plugin can be running only in one world or be linked with multiple worlds, or it can be running in multiple instances in difrent worlds. 

Even though I'm making this for myself I'm very open for all comments or even contribution to this project, maybe it can turn out to something good.