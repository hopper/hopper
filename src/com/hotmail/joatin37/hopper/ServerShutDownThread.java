package com.hotmail.joatin37.hopper;

import jline.Terminal;
import net.minecraft.server.MinecraftServer;

public class ServerShutDownThread extends Thread{
	
	 private final MinecraftServer server;
	 
	 public ServerShutDownThread(MinecraftServer server) {
		 this.server = server;
		 }
	 @Override
	 public void run() {
		 try {
			 server.initiateShutdown();
			 } catch (Exception ex) {
				 ex.printStackTrace();
				 } finally {
					 try {
						 server.reader.getTerminal();
						Terminal.resetTerminal();
						 } catch (Exception e) {
							 
						 }
					 }
		 }
}
