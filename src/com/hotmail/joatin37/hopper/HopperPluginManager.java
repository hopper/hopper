package com.hotmail.joatin37.hopper;

import java.io.File;

import net.minecraft.server.MinecraftServer;

import com.hotmail.joatin37.hopperapi.PluginManager;

public class HopperPluginManager implements PluginManager{
	
	private final File plugindirectory;
	private final File pluginsettingsdirectory;
	private final File plugindatadirectory;

	public HopperPluginManager(MinecraftServer server){
		pluginsettingsdirectory = new File(HopperServer.PLUGINSETTINGSDIRECTORY, "pluginsettings.xml");
		plugindirectory = null;
		plugindatadirectory = null;
	}
	
	public void init(){
		
	}
	
}
