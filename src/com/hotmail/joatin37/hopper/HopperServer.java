package com.hotmail.joatin37.hopper;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.minecraft.server.MinecraftServer;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import com.hotmail.joatin37.hopperapi.HopperPlugin;
import com.hotmail.joatin37.hopperapi.Server;

public class HopperServer implements Server{
	
	private final String PLUGINDIRECTORY = "plugins";
	public final static String PLUGINSETTINGSDIRECTORY = "";
	public File container;
	private MinecraftServer mserver;
	public void Shutdown(){
		//TODO
	}
	
	public HopperServer(MinecraftServer server){
		mserver=server;
	}
	
	public MinecraftServer getServer(){
		return mserver;
	}
	
	 public File getWorldFolder() {
		 if (this.getServer().anvilFile != null) {
			 return this.getServer().anvilFile;
			 }
		 if (container == null) {
			 container = new File(".");
			 }
		 return container;
		 }
}
