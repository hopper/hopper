package com.hotmail.joatin37.hopper;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import com.hotmail.joatin37.hopperapi.PluginDescription;
import com.hotmail.joatin37.hopperapi.Server;
import com.hotmail.joatin37.hopperapi.Version;

public class BasePlugin {
	private String pluginname;
	private Version version;
	private String[] worlds;
	private Server server;
	private PluginDescription description;
	private Map<String, File> datafolders;
	
	
	protected void Initialize (Server server, PluginDescription description, String[] worlds, Map<String, File> datafolders){
		this.server=server;
		this.description=description;
		this.worlds=worlds;
		this.datafolders=datafolders;
	}
	
	public Server getServer(){
		return server;
	}
	
	public PluginDescription getDescription(){
		return null;
	}
	
	public String[] getWorlds(){
		final String[] w = worlds.clone();
		return w;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((pluginname == null) ? 0 : pluginname.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		result = prime * result + Arrays.hashCode(worlds);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasePlugin other = (BasePlugin) obj;
		if (pluginname == null) {
			if (this.pluginname != null)
				return false;
		} else if (!pluginname.equals(other.pluginname))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (!Arrays.equals(worlds, other.worlds))
			return false;
		return true;
	}
	

}
